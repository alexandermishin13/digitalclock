#include <libutil.h>
#include <err.h>
#include <cerrno>
#include <csignal>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <tm1637.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <libgen.h>

/*
 * Displays digital time on TM1637
 * Writen by Mishin Alexander, 2018
 */

/* For libgpio it is GPIO numbers not PIN's
 * e.g. for RPI2 GPIO 20 is PIN 38, GPIO 21 is PIN 40
 */
#define CLK 20
#define DIO 21

#define SOCKDIR "/tmp/" // This programm for arm arch with sd storage
#define SOCKEXT ".sock"

#define CLOCKPOINT_ALWAYS 0
#define CLOCKPOINT_ONCE 1
#define CLOCKPOINT_TWICE 2

#define sizeof_field(type,field)  (sizeof(((type *)0)->field))

timer_t timerID;
struct pidfh *pfh;
int sock;

bool clockPoint = POINT_ON;
int8_t TimeDisp[4] = {'-'}; // Fill with not numbers for not succesful first compare
TM1637 disp(CLK, DIO); // clock, data pins

/* Parameters */
bool backgroundRun = false;
uint8_t levelBright = BRIGHT_DARKEST;
uint8_t tpsPoint = 0; // Times per second switch a point sign
char socketPath[sizeof_field(struct sockaddr_un, sun_path)];


/* Signals handler. Prepare the programm for end */
static void
termination_handler(int signum)
{
  /* Destroy timer */
  timer_delete(timerID);

  close(sock);
  unlink(socketPath);

  /* Off the display and terminate connection */
  disp.point(POINT_OFF);
  disp.displayOff();

  /* Remove pidfile and exit */
  pidfile_remove(pfh);

  exit(EXIT_SUCCESS);
}


/* timer handler. Effectively redraw the display */
static
void
timer_handler(int sig, siginfo_t *si, void *uc)
{
  time_t rawtime;
  struct tm tm, *timeinfo;
  int8_t loMin;

  /* Get local time (with timezone) */
  time (&rawtime);
  timeinfo = localtime_r(&rawtime, &tm);

  disp.point(clockPoint);

  /* Prepare new display digits array if a time change occurs
   * or use old one
   */
  loMin = timeinfo->tm_min % 10; // Check if change occurs
  if (loMin != TimeDisp[3])
  {
    TimeDisp[0] = timeinfo->tm_hour / 10;
    TimeDisp[1] = timeinfo->tm_hour % 10;
    TimeDisp[2] = timeinfo->tm_min / 10;
    TimeDisp[3] = loMin; // Calculated already

    /* Display a just prepared time array */
    disp.display(TimeDisp);
  }
  else
  {
    /* Display a clock point only with a second digit rewrite,
     * since a clock point is its segment.
     * The last saved value of second digit is ok as the time has not changed yet.
     * If changes of clock point is not required then no needs to rewrite it every second
     */
    if (tpsPoint > CLOCKPOINT_ALWAYS)
      disp.display(1, TimeDisp[1]);
  }

  /* Toggle a clock point for each timer tick if not always on */
  if (tpsPoint > CLOCKPOINT_ALWAYS)
    clockPoint = !clockPoint;
}


/* Create and start a half-second timer */
static
void
createTimer(uint8_t tps)
{
  struct sigaction sa;

  /* Set SIGRTMIN as first available realtime signal number
   * to avoid of uncompatibleties
   */
  sa.sa_flags     = SA_SIGINFO;
  sa.sa_sigaction = timer_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGRTMIN, &sa, NULL);

  struct sigevent te;
  memset(&te,0,sizeof(struct sigevent));
  te.sigev_notify          = SIGEV_SIGNAL;
  te.sigev_signo           = SIGRTMIN;
  te.sigev_value.sival_ptr = &timerID;
  timer_create(CLOCK_REALTIME, &te, &timerID);

  /* Get current time for a TIMER_ABSTIME flag */
  struct timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);

  struct itimerspec its;
  its.it_value.tv_sec     = ts.tv_sec + 1; // Align a start to next second...
  its.it_value.tv_nsec    = 0;             // ...as exactly as we could
  /* Normally the timer handler runs once a second.
   * But if we want a clock point changes two times per second then so be it.
   * And if there is no clock point changes at all, all the same once per second
   */
  if (tps == 2) {
      its.it_interval.tv_sec  = 0;
      its.it_interval.tv_nsec = 500000000L;    // A half of a second
  }
  else
  {
      its.it_interval.tv_sec  = 1;    // One second
      its.it_interval.tv_nsec = 0;
  }

  timer_settime(timerID, TIMER_ABSTIME, &its, NULL);
}

/* Print usage after mistaken params */
static
void
usage(char* program)
{
  printf("Usage:\n %s [-b] [-l <bright_level>] [-p <clock_point_mode>]\n", program);
  printf("\t a bright level: from 0-darkest to 7-brightest\n");
  printf("\t a clock point: 0-always on, 1-once per second, 2-twice per second\n");
}


/* Get a bright level value from params */
static
uint8_t
get_tpsPoint(char* nptr)
{
  int number;
  const char *errstr;

  number = strtonum(nptr, CLOCKPOINT_ALWAYS, CLOCKPOINT_TWICE, &errstr);
  if (errstr != NULL)
    errx(EXIT_FAILURE, "The clock point change mode is %s: %s (must be from %d to %d)", errstr, nptr, CLOCKPOINT_ALWAYS, CLOCKPOINT_TWICE);

  return number;
}


/* Get a bright level value from params */
static
uint8_t
get_levelBright(char* nptr)
{
  int number;
  const char *errstr;

  number = strtonum(nptr, BRIGHT_DARKEST, BRIGHTEST, &errstr);
  if (errstr != NULL)
    errx(EXIT_FAILURE, "The display bright level is %s: %s (must be from %d to %d)", errstr, nptr, BRIGHT_DARKEST, BRIGHTEST);

  return number;
}


/* Get and decode params */
void
get_param(int argc, char **argv)
{
  int opt;

  while((opt = getopt(argc, argv, "hbl:p:")) != -1)
  {
    switch(opt)
    {
      case 'b': // go to background (demonize)
        backgroundRun = true;
        break;

      case 'l': // bright level
        levelBright = get_levelBright(optarg);
        break;

      case 'p': // clock point change mode
        tpsPoint = get_tpsPoint(optarg);
        break;

      case 'h': // help request
      case '?': // unknown option...
      default:
        usage(argv[0]);
        break;
    }
  }
}


/* Demonize wrapper */
void
demonize(void)
{
  pid_t otherpid;

  /* Try to create a pidfile */
  pfh = pidfile_open(NULL, 0600, &otherpid);
  if (pfh == NULL)
  {
    if (errno == EEXIST)
      errx(EXIT_FAILURE, "Daemon already running, pid: %jd.", (intmax_t)otherpid);

    /* If we cannot create pidfile from other reasons, only warn. */
    warn("Cannot open or create pidfile");
    /*
     * Even though pfh is NULL we can continue, as the other pidfile_*
     * function can handle such situation by doing nothing except setting
     * errno to EDOOFUS.
     */
  }

  /* Try to demonize the process */
  if (daemon(0, 0) == -1)
  {
    pidfile_remove(pfh);
    errx(EXIT_FAILURE, "Cannot daemonize");
  }

  pidfile_write(pfh);
}


int
main(int argc, char **argv)
{
  char message[256];

  /* Prepare a socket */
  strcpy(socketPath, SOCKDIR);
  strcat(socketPath, basename(argv[0]));
  strcat(socketPath, SOCKEXT);

  sock = socket(AF_UNIX, SOCK_DGRAM, 0);
  if (sock < 0)
  {
    perror("opening stream socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockname;
  sockname.sun_family = AF_UNIX;
  strcpy(sockname.sun_path, socketPath);

  /* Analize params and set
   * configureOnly, levelBright, backgroundRun
   */
  get_param(argc, argv);

  /* If run as a daemon oa as a control utility*/
  if (backgroundRun)
  {
    demonize();

    /* Delete old socket */
    unlink(socketPath);

    /* Create and listen to new socket */
    if (bind(sock, (struct sockaddr *) &sockname, sizeof(struct sockaddr_un)))
    {
      perror("binding name to datagram socket");
      exit(EXIT_FAILURE);
    }

    /* Intercept signals to our function */
    if (signal (SIGINT, termination_handler) == SIG_IGN)
      signal (SIGINT, SIG_IGN);
    if (signal (SIGTERM, termination_handler) == SIG_IGN)
      signal (SIGTERM, SIG_IGN);

    /* Create connection and set brightness
       and clear a display
     */
    disp.set(levelBright);
    disp.clearDisplay();

    /* Run a half seconds redraw of the display */
    createTimer(tpsPoint);

    /* Read the socket with an endless loop */
    for(;;)
      /* Get a value and use it as a bright level */
      if(read(sock, message, 256) > 0)
      {
        levelBright = get_levelBright(message);
        disp.set(levelBright);
        TimeDisp[3] = '-'; // Set a low byte as non digit for immediately renew display
      }
  }
  else
  {
    /* Send command to the socket */
    sprintf(message, "%d", levelBright);
    if (sendto(sock, message, sizeof(message), 0, (struct sockaddr *) &sockname, sizeof(struct sockaddr_un)) < 0)
      perror("sending datagram message");

    close(sock);
  }

  exit(EXIT_SUCCESS);
}
