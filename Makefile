CFLAGS=-c -Wall -O2 -I/usr/local/include
LIBS=-ltm1637 -lgpio -lc -L/usr/local/lib
PREFIX=/usr/local
SUDO_CMD=sudo

all: digitalclock

digitalclock.o: digitalclock.cpp
	$(CXX) $(CFLAGS) digitalclock.cpp

digitalclock: digitalclock.o
	$(CXX) digitalclock.o ${LIBS} -lutil -lrt -o digitalclock

clean:
	rm *.o digitalclock test

install: digitalclock
	${SUDO_CMD} install -s -m 755 -o root -g wheel digitalclock ${PREFIX}/sbin
	${SUDO_CMD} install -m 755 -o root -g wheel ./rc.d/digitalclock ${PREFIX}/etc/rc.d


uninstall:
	-${SUDO_CMD} rm ${PREFIX}/sbin/digitalclock
	-${SUDO_CMD} rm ${PREFIX}/etc/rc.d/digitalclock

test: test.o
	$(CXX) test.o ${LIBS} -lrt -o test

test.o: test.c
	$(CXX) $(CFLAGS) test.cpp
